import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import components.AlertData;
import components.SatTelemetryData;

import java.io.FileReader;
import java.io.FileNotFoundException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class PagingMissionControl {


    public static void main(String[] args) throws FileNotFoundException {
        // "/pathToFile" must be changed below to reflect actual file path
        Scanner sc = new Scanner(new FileReader("/pathToFile"));
        String severity = "";
        String json = "";
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        ArrayList<String> alerts = new ArrayList<String>();

        ArrayList<SatTelemetryData> lowBattList = new ArrayList<SatTelemetryData>();
        ArrayList<SatTelemetryData>  highTempList = new ArrayList<SatTelemetryData>();

        while (sc.hasNextLine()) {
            SatTelemetryData satdata = new SatTelemetryData(sc.nextLine());
            if(isAlerting(satdata)){
                if(satdata.getComponent().equals("BATT")){
                    lowBattList.add(satdata);
                }
                if(satdata.getComponent().equals("TSTAT")){
                    highTempList.add(satdata);
                }
                if(checkForThree(lowBattList)){
                    AlertData alertData = new AlertData(lowBattList.get(0).getSatelliteId(), lowBattList.get(0).getComponent(), lowBattList.get(0).getTimestamp());
                    json = gson.toJson(alertData);
                    alerts.add(json);
                    lowBattList.clear();
                }
                if(checkForThree(highTempList)){
                    AlertData alertData = new AlertData(highTempList.get(0).getSatelliteId(), highTempList.get(0).getComponent(), highTempList.get(0).getTimestamp());
                    json = gson.toJson(alertData);
                    alerts.add(json);
                    highTempList.clear();
                }
            }
        }
        System.out.println(alerts);

    }
    public static boolean isAlerting(SatTelemetryData satdata){
        switch(satdata.getComponent()){
            case "BATT": return satdata.getRawValue() < satdata.getRedLowLimit();
            case "TSTAT": return satdata.getRawValue() > satdata.getRedHighLimit();
            default: return false;
        }
    }
    public static boolean checkForThree(ArrayList<SatTelemetryData> arr){
        if(arr.size() == 3){
        Integer[] satIds = {arr.get(0).getSatelliteId(), arr.get(1).getSatelliteId(), arr.get(2).getSatelliteId()};
            if(Arrays.stream(satIds).distinct().count() == 1){
                int timeDiff = Integer.parseInt(arr.get(2).getTimestamp().substring(12, 14)) - Integer.parseInt(arr.get(0).getTimestamp().substring(12, 14));
                return timeDiff < 5;
            }
        }
        return false;
    }

}
