package components;

public class SatTelemetryData {

    Integer satelliteId;
    String severity;
    String component;
    String timestamp;
    Double redLowLimit;
    Double redHighLimit;
    Double rawValue;

    public SatTelemetryData(String rawSatData){
        String[] lineArray = rawSatData.split("\\|");
        timestamp = lineArray[0];
        satelliteId = Integer.parseInt(lineArray[1]);
        redHighLimit = Double.parseDouble(lineArray[2]);
        redLowLimit = Double.parseDouble(lineArray[5]);
        rawValue = Double.parseDouble(lineArray[6]);
        component = lineArray[7];
    }
    public SatTelemetryData(Integer satID, String severity, String component, String timestamp ) {
        this.satelliteId = satID;
        this.severity = severity;
        this.component = component;
        this.timestamp = formattedDateTime(timestamp);
    }

    public String formattedDateTime(String timestamp){
        String[] ts = timestamp.split(" ");
        String formattedDate = ts[0].substring(0, 4) + "-" + ts[0].substring(4, 6) + "-" + ts[0].substring(6, 8) + "T" + "-" + ts[1] + "Z";
        return formattedDate;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }
    public Double getRawValue() {
        return rawValue;
    }

    public void setRawValue(Double rawValue) {
        this.rawValue = rawValue;
    }

    public Integer getSatelliteId() {
        return satelliteId;
    }

    public void setSatelliteId(Integer satelliteId) {
        this.satelliteId = satelliteId;
    }

    public Double getRedLowLimit() {
        return redLowLimit;
    }

    public void setRedLowLimit(Double redLowLimit) {
        this.redLowLimit = redLowLimit;
    }

    public Double getRedHighLimit() {
        return redHighLimit;
    }

    public void setRedHighLimit(Double redHighLimit) {
        this.redHighLimit = redHighLimit;
    }
}
