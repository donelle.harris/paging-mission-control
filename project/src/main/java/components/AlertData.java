package components;

public class AlertData {

    Integer satelliteId;
    String severity;
    String component;
    String timestamp;

    public AlertData(Integer satelliteId, String component, String timestamp) {
        this.satelliteId = satelliteId;
        this.severity = Severity.fromComponent(component);
        this.component = component;
        this.timestamp = formattedDateTime(timestamp);
    }

    public String formattedDateTime(String timestamp){
        String[] ts = timestamp.split(" ");
        String formattedDate = ts[0].substring(0, 4) + "-" + ts[0].substring(4, 6) + "-" + ts[0].substring(6, 8) + "T" + "-" + ts[1] + "Z";
        return formattedDate;
    }

}
