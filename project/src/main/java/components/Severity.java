package components;

public class Severity {


    public static String fromComponent(String component){
        switch (component){
            case "TSTAT": return "RED HIGH";
            case "BATT": return "RED LOW";
            default: return null;
        }
    }
}
